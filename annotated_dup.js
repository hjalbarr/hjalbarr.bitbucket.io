var annotated_dup =
[
    [ "ClosedLoop", null, [
      [ "DataCollection", "classClosedLoop_1_1DataCollection.html", "classClosedLoop_1_1DataCollection" ]
    ] ],
    [ "ComputerInteraction", null, [
      [ "UInterface", "classComputerInteraction_1_1UInterface.html", "classComputerInteraction_1_1UInterface" ]
    ] ],
    [ "ComputerInteraction2", null, [
      [ "UInterface", "classComputerInteraction2_1_1UInterface.html", "classComputerInteraction2_1_1UInterface" ]
    ] ],
    [ "Elevator_exercise", null, [
      [ "Button", "classElevator__exercise_1_1Button.html", "classElevator__exercise_1_1Button" ],
      [ "Floor", "classElevator__exercise_1_1Floor.html", "classElevator__exercise_1_1Floor" ],
      [ "MotorDriver", "classElevator__exercise_1_1MotorDriver.html", "classElevator__exercise_1_1MotorDriver" ],
      [ "TaskElevator", "classElevator__exercise_1_1TaskElevator.html", "classElevator__exercise_1_1TaskElevator" ]
    ] ],
    [ "Encoder", null, [
      [ "TaskEncoder", "classEncoder_1_1TaskEncoder.html", "classEncoder_1_1TaskEncoder" ],
      [ "User_Interface", "classEncoder_1_1User__Interface.html", "classEncoder_1_1User__Interface" ]
    ] ],
    [ "EncoderDriver", null, [
      [ "TaskEncoder", "classEncoderDriver_1_1TaskEncoder.html", "classEncoderDriver_1_1TaskEncoder" ]
    ] ],
    [ "FSM_LED", null, [
      [ "TaskLED", "classFSM__LED_1_1TaskLED.html", "classFSM__LED_1_1TaskLED" ]
    ] ],
    [ "main", null, [
      [ "DataCollection", "classmain_1_1DataCollection.html", "classmain_1_1DataCollection" ]
    ] ],
    [ "Motor", null, [
      [ "MotorDriver", "classMotor_1_1MotorDriver.html", "classMotor_1_1MotorDriver" ]
    ] ],
    [ "UI", null, [
      [ "UI", "classUI_1_1UI.html", "classUI_1_1UI" ]
    ] ]
];