var classComputerInteraction2_1_1UInterface =
[
    [ "__init__", "classComputerInteraction2_1_1UInterface.html#a3af25c53fccc13252cce6d5f86ebda57", null ],
    [ "Plot", "classComputerInteraction2_1_1UInterface.html#a8d006fd432eb446e9b6088e214844693", null ],
    [ "run", "classComputerInteraction2_1_1UInterface.html#a081cd86f776d5294ae588ad49edb0687", null ],
    [ "SendChar", "classComputerInteraction2_1_1UInterface.html#a563a92e7d4c7d1c024f29ec336aef49e", null ],
    [ "transitionTo", "classComputerInteraction2_1_1UInterface.html#a0b057d211c6e186e910df973ebce1e69", null ],
    [ "Condition", "classComputerInteraction2_1_1UInterface.html#a956eaa65f28a815ef6ba199947a83aa1", null ],
    [ "curr_time", "classComputerInteraction2_1_1UInterface.html#aa6587b2ebf60b0acc0ad2d5127b9141a", null ],
    [ "Data", "classComputerInteraction2_1_1UInterface.html#a25e28ad0d3b874480301856d92977cdd", null ],
    [ "Data_list", "classComputerInteraction2_1_1UInterface.html#a4516a9d63b544841f031a5d45c28dc28", null ],
    [ "Data_list2", "classComputerInteraction2_1_1UInterface.html#a2bc75bfb0f6bfec3ae876ecf5d1cc3be", null ],
    [ "fut_time", "classComputerInteraction2_1_1UInterface.html#a685fe1c502bf70fc858017e1e6370427", null ],
    [ "interval", "classComputerInteraction2_1_1UInterface.html#a79434e8ddf9128cf02d5f64c5a3777a7", null ],
    [ "inv", "classComputerInteraction2_1_1UInterface.html#a216df198c8dced3b7b15a44c8b4086a8", null ],
    [ "keyboard", "classComputerInteraction2_1_1UInterface.html#a717c4be883125990e07431ef12ecd597", null ],
    [ "myval", "classComputerInteraction2_1_1UInterface.html#abfe2bedf206ec66b6dcd05e530823475", null ],
    [ "next_time", "classComputerInteraction2_1_1UInterface.html#a930b192150a93015c5c6371ff7bd30a1", null ],
    [ "np", "classComputerInteraction2_1_1UInterface.html#af7200ff8715ce6258a7b59e6b2aa4c54", null ],
    [ "Omega", "classComputerInteraction2_1_1UInterface.html#ad87515ea740e875f5eac93701400d013", null ],
    [ "Omega_list", "classComputerInteraction2_1_1UInterface.html#a77b01ffd2b1ec4aa4c9a475f6f0e8e41", null ],
    [ "Omega_ref", "classComputerInteraction2_1_1UInterface.html#a5cc64e5620aeaf67d5d059dcebea2dbe", null ],
    [ "Omega_ref_list", "classComputerInteraction2_1_1UInterface.html#a8a412c92a0a9ada9a2e5990ad0e30848", null ],
    [ "Performance", "classComputerInteraction2_1_1UInterface.html#a3a5a10d56b3c7df5387c2764e5a559a5", null ],
    [ "Performance_list", "classComputerInteraction2_1_1UInterface.html#ac66bc9e3fae54b5da439727bfb4dbad3", null ],
    [ "plt", "classComputerInteraction2_1_1UInterface.html#a9aecce0a9a8473a78e660c8c59d1c9c8", null ],
    [ "Position", "classComputerInteraction2_1_1UInterface.html#afbe09ba0fa20e33bf0787e1e66277d41", null ],
    [ "Position_list", "classComputerInteraction2_1_1UInterface.html#add8ef30f10c4c526d9e0a66597ca18e1", null ],
    [ "Position_ref", "classComputerInteraction2_1_1UInterface.html#a6d591f72553bda0cc8ce74008c70c662", null ],
    [ "Position_ref_list", "classComputerInteraction2_1_1UInterface.html#ae43b38645ba59bec410accf3a0197c05", null ],
    [ "ref", "classComputerInteraction2_1_1UInterface.html#adfef4e4fc8c4410b87e6dd9b718fac5d", null ],
    [ "run", "classComputerInteraction2_1_1UInterface.html#ae73f173d22f5a54cc520aa1bbfe11191", null ],
    [ "ser", "classComputerInteraction2_1_1UInterface.html#a5d03f597c351d64cf0a2a235e349134b", null ],
    [ "start_time", "classComputerInteraction2_1_1UInterface.html#a90c71531d42f3cb555de5d605a3c54c3", null ],
    [ "state", "classComputerInteraction2_1_1UInterface.html#ab0c27c95cad6ac2d93c727d3e9780c9d", null ],
    [ "Time", "classComputerInteraction2_1_1UInterface.html#a5fc054e2cab653b2686211da8e284df2", null ],
    [ "Time_list", "classComputerInteraction2_1_1UInterface.html#a0a83f8576fe451c5da5b55e05b8d2326", null ]
];