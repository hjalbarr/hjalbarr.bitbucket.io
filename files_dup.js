var files_dup =
[
    [ "ClosedLoop.py", "ClosedLoop_8py.html", [
      [ "DataCollection", "classClosedLoop_1_1DataCollection.html", "classClosedLoop_1_1DataCollection" ]
    ] ],
    [ "ComputerInteraction.py", "ComputerInteraction_8py.html", "ComputerInteraction_8py" ],
    [ "ComputerInteraction2.py", "ComputerInteraction2_8py.html", "ComputerInteraction2_8py" ],
    [ "Elevator_exercise.py", "Elevator__exercise_8py.html", [
      [ "TaskElevator", "classElevator__exercise_1_1TaskElevator.html", "classElevator__exercise_1_1TaskElevator" ],
      [ "Button", "classElevator__exercise_1_1Button.html", "classElevator__exercise_1_1Button" ],
      [ "Floor", "classElevator__exercise_1_1Floor.html", "classElevator__exercise_1_1Floor" ],
      [ "MotorDriver", "classElevator__exercise_1_1MotorDriver.html", "classElevator__exercise_1_1MotorDriver" ]
    ] ],
    [ "Elevator_exercise_main_sec_02.py", "Elevator__exercise__main__sec__02_8py.html", "Elevator__exercise__main__sec__02_8py" ],
    [ "Encoder.py", "Encoder_8py.html", [
      [ "TaskEncoder", "classEncoder_1_1TaskEncoder.html", "classEncoder_1_1TaskEncoder" ],
      [ "User_Interface", "classEncoder_1_1User__Interface.html", "classEncoder_1_1User__Interface" ]
    ] ],
    [ "Encoder_main.py", "Encoder__main_8py.html", "Encoder__main_8py" ],
    [ "EncoderDriver.py", "EncoderDriver_8py.html", [
      [ "TaskEncoder", "classEncoderDriver_1_1TaskEncoder.html", "classEncoderDriver_1_1TaskEncoder" ]
    ] ],
    [ "Fibonacci_sequency.py", "Fibonacci__sequency_8py.html", "Fibonacci__sequency_8py" ],
    [ "FSM_LED.py", "FSM__LED_8py.html", "FSM__LED_8py" ],
    [ "FSM_LED_main.py", "FSM__LED__main_8py.html", "FSM__LED__main_8py" ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "Shares.py", "Shares_8py.html", "Shares_8py" ],
    [ "UI.py", "UI_8py.html", "UI_8py" ]
];